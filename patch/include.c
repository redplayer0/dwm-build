/* Bar functionality */
#include "bar_indicators.c"
#include "bar_tagicons.c"

#include "bar_ewmhtags.c"
#include "combo.c"
#include "bar_ltsymbol.c"
#include "bar_status.c"
#include "bar_statuscmd.c"
#include "bar_tags.c"
#include "bar_wintitle.c"
#include "bar_awesomebar.c"
#include "bar_wintitleactions.c"

/* Other patches */
#include "aspectresize.c"
#include "attachx.c"
#include "cyclelayouts.c"
#include "floatpos.c"
#include "moveresize.c"
#include "pertag.c"
#include "restartsig.c"
#include "shiftviewclients.c"
#include "sticky.c"
#include "togglefullscreen.c"
#include "vanitygaps.c"
/* Layouts */
#include "layout_facts.c"
#include "layout_monocle.c"
#include "layout_tile.c"

