/* Bar functionality */
#include "bar_indicators.h"
#include "bar_tagicons.h"

#include "bar_ewmhtags.h"
#include "combo.h"
#include "bar_ltsymbol.h"
#include "bar_status.h"
#include "bar_statuscmd.h"
#include "bar_tags.h"
#include "bar_wintitle.h"
#include "bar_awesomebar.h"
#include "bar_wintitleactions.h"

/* Other patches */
#include "aspectresize.h"
#include "attachx.h"
#include "cyclelayouts.h"
#include "floatpos.h"
#include "moveresize.h"
#include "pertag.h"
#include "restartsig.h"
#include "shiftviewclients.h"
#include "sticky.h"
#include "togglefullscreen.h"
#include "vanitygaps.h"
/* Layouts */
#include "layout_monocle.h"
#include "layout_tile.h"

